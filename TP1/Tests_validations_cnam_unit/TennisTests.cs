﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestsValidationsCnam.Application;

namespace TestsValidationsCnam.UnitTests
{
    public class TennisTests
    {
        [Theory]
        [InlineData(-1, 0)]
        [InlineData(0, -1)]
        [InlineData(-1, -1)]
        public void GenerateInvalidValues(int player1, int player2)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Tennis.Score(player1, player2));
        }

        [Theory]
        [InlineData(0, 0, "Love - Love")]
        [InlineData(1, 0, "Fifteen - Love")]
        [InlineData(2, 0, "Thirty - Love")]
        [InlineData(3, 0, "Forty - Love")]
        [InlineData(4, 0, "Win: Player 1")]
        [InlineData(0, 1, "Love - Fifteen")]
        [InlineData(0, 2, "Love - Thirty")]
        [InlineData(0, 3, "Love - Forty")]
        [InlineData(0, 4, "Win: Player 2")]
        [InlineData(1, 1, "Fifteen - Fifteen")]
        [InlineData(2, 2, "Thirty - Thirty")]
        [InlineData(3, 3, "Deuce")]
        [InlineData(4, 3, "FortyA - Forty")]
        [InlineData(3, 4, "Forty - FortyA")]
        [InlineData(4, 4, "Deuce")]
        [InlineData(6, 4, "Win: Player 1")]
        [InlineData(4, 6, "Win: Player 2")]
        public void ScoreValidValues(int player1, int player2, string expected)
        {
            // Act
            string resultValid = Tennis.Score(player1, player2);
            // Assert
            Assert.Equal(expected, resultValid);
        }
    }
}
