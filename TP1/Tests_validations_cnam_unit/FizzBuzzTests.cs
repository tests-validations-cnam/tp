﻿using TestsValidationsCnam.Application;
using System.Collections;
using System;

namespace TestsValidationsCnam.UnitTests
{
    public class FizzBuzzTests
    {
        [Theory]
        [InlineData(14)]
        [InlineData(151)]
        public void GenerateInvalidValues(int n)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => FizzBuzz.Generate(n));
        }
        [Theory]
        [InlineData(15, "12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz")]
        public void GenerateValid(int n, string expected)
        {
            // Act
            string resultValid = FizzBuzz.Generate(n);
            // Assert
            Assert.Equal(expected, resultValid);
        }
    }
}
