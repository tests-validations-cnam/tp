﻿using TestsValidationsCnam.Application;

namespace TestsValidationsCnam.UnitTests
{
    public class ImpotsTests
    {
        internal static readonly int PRECISION = 2;
        [Theory]
        [InlineData(-1)]
        public void GenerateInvalidValues(int n)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Impots.Generate(n));
        }

        [Theory]
        [InlineData(9000, 0, 0)]
        [InlineData(12000, 1.76d, 211.53d)]
        [InlineData(25000, 6.57d, 1641.53d)]
        [InlineData(50000, 13.51d, 6756.08d)]
        [InlineData(100000, 19.98d, 19982.33d)]
        [InlineData(200000, 28.79d, 57577.57d)]
        public void GenerateValidValues(int n, double expectedPercentage, double expectedValue)
        {
            // Act
            Tuple<double, double> resultValid = Impots.Generate(n);
            // Assert
            Assert.Equal(expectedPercentage, resultValid.Item1, PRECISION);
            Assert.Equal(expectedValue, resultValid.Item2, PRECISION);
        }
    }
}
