using TestsValidationsCnam.Application;

namespace Tests_validations_cnam_interface
{
    public partial class Form1 : Form
    {
        int playerOneScore = 0;
        int playerTwoScore = 0;
        public Form1()
        {
            InitializeComponent();
            label1.Text = Tennis.Score(playerOneScore, playerTwoScore);
        }

        private void Form1_Update(object sender, EventArgs e)
        {
            label1.Text = Tennis.Score(playerOneScore, playerTwoScore);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            playerOneScore++;
            Form1_Update(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            playerTwoScore++;
            Form1_Update(sender, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            playerOneScore = 0;
            playerTwoScore = 0;
            Form1_Update(sender, e);
        }
    }
}