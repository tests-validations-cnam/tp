﻿
using TestsValidationsCnam.Application;

static void InputFizzBuzz()
{
    Console.WriteLine("Please enter a number between 15 and 150 :");
    bool processing = true;
    while(processing)
    {
        try
        {
            int number = int.Parse(Console.ReadLine());
            Console.WriteLine(FizzBuzz.Generate(number));
            processing = false;
        }
        catch (ArgumentOutOfRangeException e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
static void InputImpot()
{
    Console.WriteLine("Please enter a positive number");
    bool processing = true;
    while (processing)
    {
        try
        {
            double number = double.Parse(Console.ReadLine());
            Tuple<double, double> result = Impots.Generate(number);
            Console.WriteLine($"Percentage : {result.Item1} %");
            Console.WriteLine($"Tax to pay: {result.Item2} €");
            processing = false;
        }
        catch (ArgumentOutOfRangeException e)
        {
            Console.WriteLine(e.Message);
        }
    }
}

Console.WriteLine("Which app do you want to use?");
Console.WriteLine("1. Fizzbuzz");
Console.WriteLine("2. Tax calculator");
Console.Write("Enter the number of the app you want to use: ");
int choice = int.Parse(Console.ReadLine());

while (choice != 1 && choice != 2)
{
    Console.WriteLine("Please enter a valid number");
    Console.Write("Enter the number of the app you want to use: ");
    choice = int.Parse(Console.ReadLine());
}
switch (choice)
{
    case 1:
        InputFizzBuzz();
        break;
    case 2:
        InputImpot();
        break;
    default:
        Console.WriteLine("Invalid choice");
        break;
}

