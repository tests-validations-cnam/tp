﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsValidationsCnam.Application
{
    public class FizzBuzz
    {
        private static readonly int MINIMUM = 15;
        private static readonly int MAXIMUM = 150;
        private static readonly SortedList SORTEDLISTMULTIPLES = new()
        {
            { "3", "Fizz" },
            { "5", "Buzz" },
        };
        private static string ConcatenateFizzRules(int n)
        {
            string result = string.Empty;
            foreach (DictionaryEntry entry in SORTEDLISTMULTIPLES)
            {
                if (n % int.Parse((string)entry.Key) == 0)
                {
                result += entry.Value;
                }
            }
            if (result == string.Empty)
            {
                result = n.ToString();
            }
            return result;
        }

        private static string CreateFizzResult(int n)
        {
            string result = string.Empty;
            for (int i = 1; i <= n; i++)
            {
                result += ConcatenateFizzRules(i);
            }
            return result;
        }
        private static void VerifyParams(int n)
        {
            if (n < MINIMUM || n > MAXIMUM)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
        }
        public static string Generate(int n)
        {
            VerifyParams(n);
            return CreateFizzResult(n);
        }
    }
}
