﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsValidationsCnam.Application
{
    public class Tennis
    {
        private static readonly string DEUCE = "Deuce";
        private static readonly string ADVANTAGE = "A";
        private static readonly string WIN = "Win: Player ";
        private static readonly int DEUCE_TRIGGER = 3;
        private static void VerifyParams(int playerOneScore, int playerTwoScore)
        {
            if (playerOneScore < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(playerOneScore));
            }
            if (playerTwoScore < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(playerTwoScore));
            }
        }
        private static string GetScore(int playerScore)
        {
            return playerScore switch
            {
                0 => "Love",
                1 => "Fifteen",
                2 => "Thirty",
                _ => "Forty"
            };
        }
        private static string ComputeScoreDiff(int playerOneScore, int playerTwoScore)
        {
            int scoreDiff = playerOneScore - playerTwoScore;
            return scoreDiff switch
            {
                1 => $"{GetScore(playerOneScore)}{ADVANTAGE} - {GetScore(playerTwoScore)}",
                -1 => $"{GetScore(playerOneScore)} - {GetScore(playerTwoScore)}{ADVANTAGE}",
                _ => $"{WIN}{(scoreDiff > 0 ? 1 : 2)}"
            };
        }
        private static string ComputeScore(int playerOneScore, int playerTwoScore)
        {
            if (playerOneScore == playerTwoScore)
            {
                return playerOneScore >= DEUCE_TRIGGER ? DEUCE : GetScore(playerOneScore) + " - " + GetScore(playerTwoScore);
            }
            if (playerOneScore > DEUCE_TRIGGER || playerTwoScore > DEUCE_TRIGGER)
            {
                return ComputeScoreDiff(playerOneScore, playerTwoScore);
            }
            return $"{GetScore(playerOneScore)} - {GetScore(playerTwoScore)}";
        }
        public static string Score(int playerOneScore, int playerTwoScore)
        {
            VerifyParams(playerOneScore, playerTwoScore);
            return ComputeScore(playerOneScore, playerTwoScore);
        }
    }
}
