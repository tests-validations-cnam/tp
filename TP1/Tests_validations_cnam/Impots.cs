﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsValidationsCnam.Application
{
    public class Impots
    {
        internal static readonly SortedList<double, double> STEPS = new()
        {
            { 10077, 0 },
            { 27478, 11 },
            { 78570, 30 },
            { 168994, 41 },
            { double.MaxValue, 45 }
        };
        private static void VerifyParams(double n)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }
        }
        private static double ComputeValue(double n)
        {
            double result = 0;
            double tempFingerPrint = n;
            foreach (KeyValuePair<double, double> step in STEPS)
            {
                if (n <= step.Key)
                {
                    result += tempFingerPrint * step.Value / 100;
                    break;
                }
                tempFingerPrint -= step.Key;
                result += step.Key * step.Value / 100;
            }
            return result;
        }
        private static double ComputePercentage(double n, double valueTax)
        {
            return valueTax * 100 / n;
        }

        private static Tuple<double, double> RoundValues(double percentage, double valueTax)
        {
            return new Tuple<double, double>(Math.Round(percentage, 2), Math.Round(valueTax, 2));
        }

        public static Tuple<double, double> Generate(double n)
        {
            VerifyParams(n);
            double value = ComputeValue(n);
            double percentage = ComputePercentage(n, value);
            return RoundValues(percentage, value);
        }
    }
}
