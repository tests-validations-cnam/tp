﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestValidationsCnamTP2Library;
using TestValidationsCnamTP2Library.Enums;
using TestValidationsCnamTP2Library.Records;

namespace TestValidationsCnamTP2Tests
{
    public class RealEstateCalculatorTests
    {
        public static IEnumerable<object[]> TestDataCalculateInsuranceRate()
        {
            yield return new object[] { new List<InsuranceCategory>(), 0.003 };
            yield return new object[] { new List<InsuranceCategory> { "Sportif" }, 0.0025 };
            yield return new object[] { new List<InsuranceCategory> { "Smoker" }, 0.0045 };
            yield return new object[] { new List<InsuranceCategory> { "Heart problems" }, 0.006 };
            yield return new object[] { new List<InsuranceCategory> { "IT engineer" }, 0.0025 };
            yield return new object[] { new List<InsuranceCategory> { "Fighter pilot" }, 0.0045 };
            yield return new object[] { new List<InsuranceCategory> { "Sportif", "Smoker" }, 0.004 };
        }
        public static IEnumerable<object[]> TestDataCalculateTotalMonthlyPayment()
        {
            yield return new object[] { 50000, 300, InterestRateCategoryEnum.Good, new List<InsuranceCategory> { }, 249.61 };
            yield return new object[] { 75000, 300, InterestRateCategoryEnum.Good, new List<InsuranceCategory> { }, 374.41 };
            yield return new object[] { 50000, 250, InterestRateCategoryEnum.Good, new List<InsuranceCategory> { }, 279.20 };
            yield return new object[] { 50000, 300, InterestRateCategoryEnum.ReallyGood, new List<InsuranceCategory> { }, 246.5 };
            yield return new object[] { 50000, 300, InterestRateCategoryEnum.Good, new List<InsuranceCategory> { "Sportif" }, 247.53 };
        }
        public static IEnumerable<object[]> TestDataCalculatePaymentDoneAfterTenYears()
        {
            yield return new object[] { 50000, 300, InterestRateCategoryEnum.Good, new List<InsuranceCategory> { }, 29953.20 };
            yield return new object[] { 75000, 300, InterestRateCategoryEnum.Good, new List<InsuranceCategory> { }, 44929.20 };
            yield return new object[] { 50000, 250, InterestRateCategoryEnum.Good, new List<InsuranceCategory> { }, 33504 };
            yield return new object[] { 50000, 300, InterestRateCategoryEnum.ReallyGood, new List<InsuranceCategory> { }, 29580 };
            yield return new object[] { 50000, 300, InterestRateCategoryEnum.Good, new List<InsuranceCategory> { "Sportif" }, 29703.60 };
        }
        [Theory]
        [InlineData(50000, 300, InterestRateCategoryEnum.Good, 237.11)]
        [InlineData(50000, 250, InterestRateCategoryEnum.ReallyGood, 263.20)]
        [InlineData(100000, 190, InterestRateCategoryEnum.Excellent, 629.86)]
        [InlineData(100000, 130, InterestRateCategoryEnum.Good, 884.85)]
        [InlineData(150000, 108, InterestRateCategoryEnum.ReallyGood, 1543.64)]
        public void CalculateMonthlyPayment(double loanAmount, double termInMonths, InterestRateCategoryEnum interestRateCategoryEnum, double expected)
        {
            double actual = RealEstateCalculator.CalculateMonthlyPayment(loanAmount, termInMonths, interestRateCategoryEnum);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(49999, 108, InterestRateCategoryEnum.Good)]
        [InlineData(50000, 107, InterestRateCategoryEnum.Good)]
        [InlineData(50000, 301, InterestRateCategoryEnum.Good)]
        public void CalculateMonthlyPayment_InvalidValues_ThrowArgumentOutOfRangeException(double loanAmount, double termInMonths, InterestRateCategoryEnum interestRateCategoryEnum)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => RealEstateCalculator.CalculateMonthlyPayment(loanAmount, termInMonths, interestRateCategoryEnum));
        }

        [Theory]
        [MemberData(nameof(TestDataCalculateInsuranceRate))]
        public void CalculateInsuranceRate(List<InsuranceCategory> insuranceCategories, double expected)
        {
            double actual = RealEstateCalculator.CalculateInsuranceRate(insuranceCategories.ToList());
            Assert.Equal(expected, actual);
        }

        [Theory]
        [MemberData(nameof(TestDataCalculateTotalMonthlyPayment))]
        public void CalculateTotalMonthlyPayment(double loanAmount, double termInMonths, InterestRateCategoryEnum interestRateCategoryEnum, List<InsuranceCategory> insuranceCategories, double expected)
        {
            double actual = RealEstateCalculator.CalculateTotalMonthlyPayment(loanAmount, termInMonths, interestRateCategoryEnum, insuranceCategories);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(50000, 0.003, 12.50)]
        [InlineData(50000, 0.006, 25)]
        [InlineData(150000, 0.003, 37.5)]
        public void CalculateMonthlyInsurancePayment(double loanAmount, double insuranceRate, double expected)
        {
            double actual = RealEstateCalculator.CalculateMonthlyInsurancePayment(loanAmount, insuranceRate);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(50000, 300, InterestRateCategoryEnum.Good, 21133)]
        [InlineData(70000, 300, InterestRateCategoryEnum.Good, 29585)]
        [InlineData(50000, 250, InterestRateCategoryEnum.Good, 16675)]
        [InlineData(50000, 250, InterestRateCategoryEnum.ReallyGood, 15800)]
        public void CalculateTotalInterestPayment(double loanAmount, double termInMonths, InterestRateCategoryEnum interestRateCategoryEnum, double expected)
        {
            double actual = RealEstateCalculator.CalculateTotalInterestPayment(loanAmount, termInMonths, interestRateCategoryEnum);
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(50000, 0.003, 300, 3750)]
        [InlineData(150000, 0.003, 300, 11250)]
        [InlineData(50000, 0.006, 300, 7500)]
        [InlineData(50000, 0.003, 250, 3125)]
        public void CalculateTotalInsurancePayment(double loanAmount, double insuranceRate, double termInMonths, double expected)
        {
            double actual = RealEstateCalculator.CalculateTotalInsurancePayment(loanAmount, insuranceRate, termInMonths);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [MemberData(nameof(TestDataCalculatePaymentDoneAfterTenYears))]
        public void CalculatePaymentDoneAfterTenYears(double loanAmount, double termInMonths, InterestRateCategoryEnum interestRateCategoryEnum, List<InsuranceCategory> insuranceCategories, double expected)
        {
            double actual = RealEstateCalculator.CalculatePaymentDoneAfterTenYears(loanAmount, termInMonths, interestRateCategoryEnum, insuranceCategories);
            Assert.Equal(expected, actual);
        }       
    }
}
