﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestValidationsCnamTP2Library.Records
{
    public record Amount
    {
        public double Value { get; init; }
        public Amount(double value)
        {
            if (value < 50000)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "The amount must be greater than 50000");
            }
            Value = value;
        }

        public static implicit operator double(Amount amount) => amount.Value;
        public static implicit operator Amount(double amount) => new(amount);
    }
}
