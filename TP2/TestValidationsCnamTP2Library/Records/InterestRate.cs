﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestValidationsCnamTP2Library.Records
{
    public record InterestRate
    {
        public double Value { get; init; }
        public InterestRate(double value)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "The interest rate must be greater than 0");
            }
            Value = value;
        }

        public static implicit operator double(InterestRate interestRate) => interestRate.Value;
        public static implicit operator InterestRate(double interestRate) => new(interestRate);
    }
}
