﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestValidationsCnamTP2Library.Records
{
    public record TermMonth
    {
        public static readonly int MIN_VALUE = 108;
        public static readonly int MAX_VALUE = 300;
        public double Value { get; init; }
        public TermMonth(double value)
        {
            if (value < MIN_VALUE)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "The term in months must be greater than 108");
            }
            if (value > MAX_VALUE)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "The term in months must be less than 300");
            }
            Value = value;
        }

        public static implicit operator double(TermMonth termMonth) => termMonth.Value;
        public static implicit operator TermMonth(double termMonth) => new(termMonth);
    }
}
