﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestValidationsCnamTP2Library.Records
{
    public record InsuranceRate
    {
        private static readonly double BASE_INSURANCE_RATE = 0.003;
        public double Value { get; init; }
        public InsuranceRate()
        {
            Value = BASE_INSURANCE_RATE;
        }
        public InsuranceRate(double value)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "The insurance rate must be greater than 0");
            }
            Value = value;
        }

        public static implicit operator double(InsuranceRate insuranceRate) => insuranceRate.Value;
        public static implicit operator InsuranceRate(double insuranceRate) => new(insuranceRate);
    }
}
