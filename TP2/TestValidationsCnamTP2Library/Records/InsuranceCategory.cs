﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestValidationsCnamTP2Library.Records
{
    public record InsuranceCategory
    {
        public static readonly Dictionary<string, double> availableValues = new()
        {
            { "Sportif", -0.0005 },
            { "Smoker", 0.0015 },
            { "Heart problems", 0.003 },
            { "IT engineer",  -0.0005 },
            { "Fighter pilot", 0.0015 }
        };
        public double Value { get; init; }
        public InsuranceCategory(string value)
        {
            if (!availableValues.ContainsKey(value))
            {
                throw new ArgumentOutOfRangeException(nameof(value), "The insurance category is not available");
            }
            Value = availableValues[value];
        }

        public static implicit operator double(InsuranceCategory insuranceCategory) => insuranceCategory.Value;
        public static implicit operator InsuranceCategory(string insuranceCategory) => new(insuranceCategory);
    }
}
