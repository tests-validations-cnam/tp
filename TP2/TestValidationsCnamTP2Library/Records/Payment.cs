﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestValidationsCnamTP2Library.Records
{
    public record Payment
    {
        public double Value { get; init; }
        public Payment(double value)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "The payment must be greater than 0");
            }
            Value = value;
        }

        public static implicit operator double(Payment payment) => payment.Value;
        public static implicit operator Payment(double payment) => new(payment);
        // To String
        public override string ToString() => Value.ToString();
    }
}
