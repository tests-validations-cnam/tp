﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestValidationsCnamTP2Library.Enums
{
    public enum InterestRateCategoryEnum
    {
        Unknown,
        Good,
        ReallyGood,
        Excellent
    }
}
