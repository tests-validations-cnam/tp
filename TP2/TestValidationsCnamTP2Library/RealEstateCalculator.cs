﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestValidationsCnamTP2Library.Enums;
using TestValidationsCnamTP2Library.Records;

namespace TestValidationsCnamTP2Library
{
    public class RealEstateCalculator
    {
        private static readonly int NUMBER_OF_MONTHS_IN_YEAR = 12;

        private static readonly Dictionary<TermMonth, Dictionary<InterestRateCategoryEnum, InsuranceRate>> interestRatesTests = new()
        {
            { TermMonth.MAX_VALUE, new Dictionary<InterestRateCategoryEnum, InsuranceRate> { { InterestRateCategoryEnum.Good, 0.03 }, { InterestRateCategoryEnum.ReallyGood, 0.0288 }, { InterestRateCategoryEnum.Excellent, 0.0245 } } },
            { 240, new Dictionary<InterestRateCategoryEnum, InsuranceRate> { { InterestRateCategoryEnum.Good, 0.029 }, { InterestRateCategoryEnum.ReallyGood, 0.0276 }, { InterestRateCategoryEnum.Excellent, 0.0233 } } },
            { 180, new Dictionary<InterestRateCategoryEnum, InsuranceRate> { { InterestRateCategoryEnum.Good, 0.0279 }, { InterestRateCategoryEnum.ReallyGood, 0.0267 }, { InterestRateCategoryEnum.Excellent, 0.0233 } } },
            { 120, new Dictionary<InterestRateCategoryEnum, InsuranceRate> { { InterestRateCategoryEnum.Good, 0.0263 }, { InterestRateCategoryEnum.ReallyGood, 0.0247 }, { InterestRateCategoryEnum.Excellent, 0.0215 } } },
            { TermMonth.MIN_VALUE, new Dictionary<InterestRateCategoryEnum, InsuranceRate> { { InterestRateCategoryEnum.Good, 0.0256 }, { InterestRateCategoryEnum.ReallyGood, 0.0237 }, { InterestRateCategoryEnum.Excellent, 0.0210 } } }
        };
        public static InsuranceRate CalculateInsuranceRate(IEnumerable<InsuranceCategory> insuranceCategories)
        {
            InsuranceRate insuranceRate = new();
            foreach (InsuranceCategory insuranceCategory in insuranceCategories)
            {
                insuranceRate += insuranceCategory;
            }
            return Math.Round(insuranceRate, 4);
        }
        
        public static InterestRate CalculateInterestRate(TermMonth termInMonths, InterestRateCategoryEnum interestRateCategory)
        {
            foreach (KeyValuePair<TermMonth, Dictionary<InterestRateCategoryEnum, InsuranceRate>> interestRate in interestRatesTests)
            {
                if (termInMonths >= interestRate.Key)
                {
                    return new InterestRate(interestRate.Value[interestRateCategory]);
                }
            }
            throw new ArgumentOutOfRangeException(nameof(termInMonths), "The term in months is not supported.");
        }

        public static Payment CalculateMonthlyInsurancePayment(Amount loanAmount, InsuranceRate insuranceRate)
        {
            return Math.Round(loanAmount * insuranceRate / NUMBER_OF_MONTHS_IN_YEAR, 2);
        }

        public static Payment CalculateMonthlyPayment(Amount loanAmount, TermMonth termInMonths, InterestRateCategoryEnum interestRateCategoryEnum)
        {
            InterestRate monthlyInterestRate = CalculateInterestRate(termInMonths, interestRateCategoryEnum) / NUMBER_OF_MONTHS_IN_YEAR;
            Payment monthlyPayment = loanAmount * monthlyInterestRate / (1 - Math.Pow(1 + monthlyInterestRate, -termInMonths));
            return Math.Round(monthlyPayment, 2);
        }

        public static Payment CalculateTotalInterestPayment(Amount loanAmount, TermMonth termInMonths, InterestRateCategoryEnum interestRateCategoryEnum)
        {
            return Math.Round(CalculateMonthlyPayment(loanAmount, termInMonths, interestRateCategoryEnum) * termInMonths - loanAmount, 2);
        }

        public static Payment CalculateTotalInsurancePayment(Amount loanAmount, InsuranceRate insuranceRate, TermMonth termInMonths)
        {
            return Math.Round(CalculateMonthlyInsurancePayment(loanAmount, insuranceRate) * termInMonths, 2);
        }

        public static Payment CalculateTotalMonthlyPayment(Amount loanAmount, TermMonth termInMonths, InterestRateCategoryEnum interestRateCategoryEnum, List<InsuranceCategory> insuranceCategories)
        {
            InsuranceRate insuranceRate = CalculateInsuranceRate(insuranceCategories);
            return Math.Round(CalculateMonthlyPayment(loanAmount, termInMonths, interestRateCategoryEnum) + CalculateMonthlyInsurancePayment(loanAmount, insuranceRate), 2);
        }
        public static Payment CalculatePaymentDoneAfterTenYears(Amount loanAmount, TermMonth termInMonths, InterestRateCategoryEnum interestRateCategoryEnum, List<InsuranceCategory> insuranceCategories)
        {
            double totalMonthlyPayment = CalculateTotalMonthlyPayment(loanAmount, termInMonths, interestRateCategoryEnum, insuranceCategories);
            return Math.Round(totalMonthlyPayment * Math.Min(termInMonths, 120), 2);
        }
    }
}
