using TestValidationsCnamTP2Library;
using TestValidationsCnamTP2Library.Enums;
using TestValidationsCnamTP2Library.Records;

namespace TestValidationsCnamTP2View
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_calculate_Click(object sender, EventArgs e)
        {
            Amount loanAmount = double.Parse(tb_la.Text);
            TermMonth loanTerm = double.Parse(tb_ty.Text) * 12;
            InterestRateCategoryEnum interestRateCategory;
            switch (comboBox1.SelectedItem)
            {
                case "Excellent":
                    interestRateCategory = InterestRateCategoryEnum.Excellent;
                    break;
                case "Really Good":
                    interestRateCategory = InterestRateCategoryEnum.ReallyGood;
                    break;
                default:
                    interestRateCategory = InterestRateCategoryEnum.Good;
                    break;
            }
            List<InsuranceCategory> insuranceRates = new();
            foreach (var item in clb_IS.CheckedItems)
            {
                insuranceRates.Add(new(item.ToString()));
            }
            Payment monthlyPayment = RealEstateCalculator.CalculateMonthlyPayment(loanAmount, loanTerm, interestRateCategory);
            MessageBox.Show($"The total monthly payment is {monthlyPayment}.");
            Payment monthlyInsurancePayment = RealEstateCalculator.CalculateMonthlyInsurancePayment(loanAmount, RealEstateCalculator.CalculateInsuranceRate(insuranceRates));
            MessageBox.Show($"The total monthly insurance payment is {monthlyInsurancePayment}.");
            Payment totalInterestPayment = RealEstateCalculator.CalculateTotalInterestPayment(loanAmount, loanTerm, interestRateCategory);
            MessageBox.Show($"The total interest payment is {totalInterestPayment}.");
            Payment totalInsurancePayment = RealEstateCalculator.CalculateTotalInsurancePayment(loanAmount, RealEstateCalculator.CalculateInsuranceRate(insuranceRates), loanTerm);
            MessageBox.Show($"The total insurance payment is {totalInsurancePayment}.");
            Payment totalPaymentAfterTenYears = RealEstateCalculator.CalculatePaymentDoneAfterTenYears(loanAmount, loanTerm, interestRateCategory, insuranceRates);
            MessageBox.Show($"The total payment after ten years is {totalPaymentAfterTenYears}.");

        }
    }
}