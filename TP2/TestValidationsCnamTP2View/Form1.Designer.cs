﻿namespace TestValidationsCnamTP2View
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tb_la = new System.Windows.Forms.TextBox();
            this.label_ty = new System.Windows.Forms.Label();
            this.tb_ty = new System.Windows.Forms.TextBox();
            this.label_it = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label_ic = new System.Windows.Forms.Label();
            this.clb_IS = new System.Windows.Forms.CheckedListBox();
            this.btn_calculate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Loan Amount:";
            // 
            // tb_la
            // 
            this.tb_la.Location = new System.Drawing.Point(89, 6);
            this.tb_la.Name = "tb_la";
            this.tb_la.Size = new System.Drawing.Size(100, 23);
            this.tb_la.TabIndex = 1;
            // 
            // label_ty
            // 
            this.label_ty.AutoSize = true;
            this.label_ty.Location = new System.Drawing.Point(223, 9);
            this.label_ty.Name = "label_ty";
            this.label_ty.Size = new System.Drawing.Size(98, 15);
            this.label_ty.TabIndex = 2;
            this.label_ty.Text = "Number of Years:";
            // 
            // tb_ty
            // 
            this.tb_ty.Location = new System.Drawing.Point(327, 6);
            this.tb_ty.Name = "tb_ty";
            this.tb_ty.Size = new System.Drawing.Size(100, 23);
            this.tb_ty.TabIndex = 3;
            // 
            // label_it
            // 
            this.label_it.AutoSize = true;
            this.label_it.Location = new System.Drawing.Point(463, 9);
            this.label_it.Name = "label_it";
            this.label_it.Size = new System.Drawing.Size(79, 15);
            this.label_it.TabIndex = 4;
            this.label_it.Text = "Interest Type: ";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Good",
            "Really Good",
            "Excellent"});
            this.comboBox1.Location = new System.Drawing.Point(548, 6);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(148, 23);
            this.comboBox1.TabIndex = 5;
            // 
            // label_ic
            // 
            this.label_ic.AutoSize = true;
            this.label_ic.Location = new System.Drawing.Point(0, 54);
            this.label_ic.Name = "label_ic";
            this.label_ic.Size = new System.Drawing.Size(123, 15);
            this.label_ic.TabIndex = 6;
            this.label_ic.Text = "Insurance Categories :";
            // 
            // clb_IS
            // 
            this.clb_IS.FormattingEnabled = true;
            this.clb_IS.Items.AddRange(new object[] {
            "Sportif",
            "Smoker",
            "Heart problems",
            "IT engineer",
            "Fighter pilot"});
            this.clb_IS.Location = new System.Drawing.Point(12, 72);
            this.clb_IS.Name = "clb_IS";
            this.clb_IS.Size = new System.Drawing.Size(141, 130);
            this.clb_IS.TabIndex = 7;
            // 
            // btn_calculate
            // 
            this.btn_calculate.Location = new System.Drawing.Point(223, 95);
            this.btn_calculate.Name = "btn_calculate";
            this.btn_calculate.Size = new System.Drawing.Size(75, 23);
            this.btn_calculate.TabIndex = 8;
            this.btn_calculate.Text = "Calculate";
            this.btn_calculate.UseVisualStyleBackColor = true;
            this.btn_calculate.Click += new System.EventHandler(this.btn_calculate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_calculate);
            this.Controls.Add(this.clb_IS);
            this.Controls.Add(this.label_ic);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label_it);
            this.Controls.Add(this.tb_ty);
            this.Controls.Add(this.label_ty);
            this.Controls.Add(this.tb_la);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private TextBox tb_la;
        private Label label_ty;
        private TextBox tb_ty;
        private Label label_it;
        private ComboBox comboBox1;
        private Label label_ic;
        private CheckedListBox clb_IS;
        private Button btn_calculate;
    }
}